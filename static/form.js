const API_URL = ""

const initForm = (form) => {

    form.addEventListener('submit', async (event) => {
        event.preventDefault()
        event.stopPropagation()

        const data = Object.fromEntries(new FormData(form))
        const response = await fetch(
            `${API_URL}/items/`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(data),
            }
        )
        console.log(response)

    }, false)
}

document.querySelectorAll('form').forEach(initForm)
