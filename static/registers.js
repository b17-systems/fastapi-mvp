const API_URL = "";

const updateRangeValue = async (elem, response) => {
    const json = await response.json();
    console.log(json);
    elem.value = json.value;
    document.getElementById(`${elem.id}-value`).innerHTML = json.value;
    return json;
}

const getRegisterValue = async (elem) => {
    const response = await fetch(
        `${API_URL}/registers/${elem.dataset.node}/${elem.dataset.register}`
    );
    return updateRangeValue(elem, response);
}
const setRegisterValue = async (elem, value) => {
    const response = await fetch(
        `${API_URL}/registers/${elem.dataset.node}/${elem.dataset.register}`,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ value }),
        }
    );
    return updateRangeValue(elem, response);
}

const initRegisterInput = async (elem) => {
    // const register = await getRegisterValue(elem);

    // if (typeof register.min === "number") elem.min = register.min;
    // if (typeof register.max === "number") elem.max = register.max;
    // if (typeof register.value === "number") elem.value = register.value;

    // elem.step = elem.step || (elem.max - elem.min) / 100;

    elem.addEventListener("change", async (event) => {
        console.log("change", event.target.value);
        setRegisterValue(elem, event.target.value);
    });
    elem.addEventListener("input", (event) => {
        document.getElementById(`${elem.id}-value`).innerHTML = event.target.value;
    });

    elem.disabled = false;
}

document.querySelectorAll('input[type="range"][data-register]').forEach(initRegisterInput);
