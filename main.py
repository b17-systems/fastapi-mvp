from datetime import datetime
import logging
from typing import Any, Optional, Union
from uuid import UUID, uuid4

from fastapi import FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, PlainTextResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from httpx import HTTPError
from pydantic import BaseModel, Field

_logger = logging.getLogger(__name__)
app = FastAPI()


class Item(BaseModel):
    form: int
    id: UUID = Field(default_factory=uuid4)
    name: str
    number: Union[int, float]
    description: str = ""


db: dict[UUID, Item] = {}

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


@app.get("/", response_class=HTMLResponse)
async def serve_main_page(request: Request):
    return templates.TemplateResponse(
        "base.html", {"request": request, "title": "FastAPI MVP"}
    )


@app.get("/form", response_class=RedirectResponse)
async def redirect_to_form_1():
    return RedirectResponse("/form/1", status_code=308)


@app.get("/form/{id}", response_class=HTMLResponse)
async def serve_form(request: Request, id: int):
    return templates.TemplateResponse("form.html", {"request": request, "id": id})


@app.get("/coffee", response_class=HTMLResponse)
async def get_coffee(request: Request):
    return templates.TemplateResponse(
        "tea.html",
        {"request": request, "status": 418},
        status_code=418,
    )


@app.get("/time", response_class=PlainTextResponse)
async def get_time(format: str = "%c"):
    now = datetime.now().astimezone()
    try:
        return now.strftime(format)
    except ValueError:
        return f"format not understood: {format!r}", 422


@app.get("/items/", response_model=list[Item])
def get_items() -> list[Item]:
    return db.values()


@app.get("/items/{id}", response_model=Item)
def get_items(id: UUID) -> Item:
    try:
        return db[id]
    except KeyError as err:
        raise HTTPException(404, f"no Item with UUID {id!s}") from err


@app.post("/items/", response_model=Item, status_code=201)
def get_items(item: Item) -> Item:
    db[item.id] = item
    return item


if __name__ == "__main__":
    import uvicorn

    _logger.info("starting as module...")

    uvicorn.run("main:app", host="0.0.0.0", port=0, reload=True)
